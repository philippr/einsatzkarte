import os
import sys
import geotiler
from geopy.geocoders import Nominatim
from PIL import Image, ImageDraw

if(len(sys.argv) < 2):
    print("Usage: python3.7 einsatzkarte.py \"Address \"")
    exit()
addr = sys.argv[1]

geolocator = Nominatim(user_agent="einsatzkarte")
location = geolocator.geocode(addr)
width = 1110
height = 508
zoom = 15
lng = location.longitude
lat = location.latitude
drawRect = False

print(location.raw)

type = location.raw['type']
addrClass = location.raw['class']
[b_lat1, b_lat2, b_lng1, b_lng2] = map(float, location.raw['boundingbox'])

if(addrClass in ['amenity','shop','highway','building','leisure']):
    zoom = 17
    if(addrClass != 'highway'):
        drawRect = True
    
map = geotiler.Map(center=(lng, lat), zoom=zoom, size=(width, height), provider='osm')
map.extent
image_base = geotiler.render_map(map)
map = geotiler.Map(center=(lng, lat), zoom=zoom, size=(width, height), provider='ofm')
map.extent
hydrants = geotiler.render_map(map)

x, y = map.rev_geocode((lng, lat))
(b_x1, b_y1) = map.rev_geocode((b_lng1, b_lat1))
(b_x2, b_y2) = map.rev_geocode((b_lng2, b_lat2))

image_base.paste(hydrants, (0,0), hydrants)
if(drawRect):
    draw = ImageDraw.Draw(image_base)
    for i in range(20,27):
        draw.rectangle((b_x1-i,b_y1-i,b_x2+i,b_y2+i), outline=(255,0,0))
#    draw.rectangle(((x-20,y-20),(x+20,y+20)), outline=(0,0,255))
    del draw
image_base.show()
image_base.save('map.png')
# os.system("display map.png")



